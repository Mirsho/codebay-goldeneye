function copyToClipboard() {
    // Get the text field
    var copyText = document.getElementById("userEmail");

    // Select the text field
    copyText.select();
    copyText.setSelectionRange(0, 99999); // For mobile devices

    // Copy the text inside the text field
    navigator.clipboard.writeText(copyText.value);

    // Alert the copied text
    alert("Copied the text: " + copyText.value);
}

/**
 * Create DOM node which can be placed on the DOM tree.
 *
 * @param {string} nodeType HTML tag for the new node
 * @param {string} nodeText Text content of the node element
 * @param {array} nodeClasess Class names for the node
 * @param {array} nodeAttributtes Array of objects consisting on { key: 'atributte-name', value: 'atributte-value'}
 * @returns DOM node.
 */
function createNode(nodeType, nodeText, nodeClasess, nodeAttributtes) {
    let node = document.createElement(nodeType);
    if (nodeText !== "" && nodeText !==null) {
        node.appendChild(document.createTextNode(nodeText));
    }
    if (nodeClasess.length > 0) {
        nodeClasess.forEach((clss) => node.classList.add(clss));
    }
    if (nodeAttributtes.length > 0) {
        nodeAttributtes.forEach((attributte) =>
            node.setAttribute(attributte.name, attributte.value)
        );
    }
    return node;
}

/**
 * Erases the DOM node passed after getting it from the DOM tree.
 *
 * @param {object} node DOM node to be erased
 */
function eraseChildNodes(node) {
    while (node.firstChild) {
        node.removeChild(node.firstChild);
    }
}

//const locationInput = document.getElementById("location");
//const departmentInput = document.getElementById("department");
//const locationsPath = "../../data/locations.json";
//const submitButton = document.getElementById("submit-button");
//// submitButton.disabled = true;
////submitButton.setAttribute("disabled", true);
//
//function generateLocations(locations) {
//    locations.forEach((location) => {
//        let locationOption = createNode("option", location.name, [], []);
//        locationInput.appendChild(locationOption);
//    });
//}
//
//document.addEventListener("DOMContentLoaded", function () {
//    fetch(locationsPath)
//            .then((response) => response.json())
//            .then((data) => generateLocations(data));
//});
//
//function generateDepartments(locations, currentLocation) {
//    let current = locations.filter((location) => {
//        return location.name === currentLocation;
//    });
//    current.departments.forEach((department) => {
//        let departmentOption = createNode("option", department.name, [], []);
//        departmentInput.appendChild(departmentOption);
//    });
//}
//
//locationInput.addEventListener("change", function () {
//    fetch(locationsPath)
//            .then((response) => response.json())
//            .then((data) => generateDepartments(data, this.value));
//});