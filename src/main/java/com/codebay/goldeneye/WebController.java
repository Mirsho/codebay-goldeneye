package com.codebay.goldeneye;

import java.text.Normalizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

@Controller
@Configuration
@PropertySource("classpath:goldeneye.properties")
public class WebController {

    @GetMapping("/")
    public String index() {
        return "redirect:/form";
    }

    @GetMapping("/form")
    public String formGet() {
        return "form";
    }

    //Get goldeneye API uri from properties
    @Value("${goldeneye.api.uri}")
    private String goldeneyeApiUri;
    
    //Preparing code integration for Java API
    @ResponseBody
    private String checkUnnapropiate(@PathVariable String input
    ) {
        String uri = goldeneyeApiUri + input;
        RestTemplate restTemplate = new RestTemplate();

        String checkedText;
        checkedText = restTemplate.getForObject(uri, String.class);

        return checkedText;
    }

    @PostMapping("/form")
    public String formPost(UserDto user, Model model
    ) {
        //Uncomment for text checking through API call
        //user.setName(checkUnnapropiate(user.getName()));
        //user.setSurname(checkUnnapropiate(user.getSurname()));

        //Get input data and normalize it
        char nameFirstLetter = normalize(user.getName()).charAt(0);
        String surname = normalize(user.getSurname());
        String department = normalize(user.getDepartment());
        String location = normalize(user.getLocation());
        String companyDomain = normalize(".goldeneye.com");

        // StringBuilder instance to compose email string with normalized user input data
        StringBuilder sbuilder = new StringBuilder();

        //Email address composition with normalized input data
        sbuilder.append(nameFirstLetter);
        sbuilder.append(surname);
        sbuilder.append('.');
        sbuilder.append(department);
        sbuilder.append('@');
        sbuilder.append(location);
        sbuilder.append(companyDomain);

        //Set generated address to user obj
        user.setEmail(sbuilder.toString());

        //Add user obj to model, so we can render it at front
        model.addAttribute("user", user);
        return "form";
    }

    private boolean checkNormalization(String input) {
        return Normalizer.isNormalized(input, Normalizer.Form.NFKD);
    }

    private String removeAccents(String input) {
        return input.replaceAll("\\p{M}", "");
    }

    private String normalize(String input) {
        if (checkNormalization(input) == false) {
            input = Normalizer.normalize(input, Normalizer.Form.NFKD);
            input = removeAccents(input);
        }
        return input.toLowerCase().replaceAll("\\s+", "");
    }
}
